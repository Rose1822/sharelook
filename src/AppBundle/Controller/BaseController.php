<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class BaseController extends Controller
{
    protected $db;
    
    public function __construct()
    {
        
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');
        $apiKey = 'AIzaSyCbtIwh1tTOh4ABa0oWNqxdjjViMa-3bic';

        $firebase = (new Factory)
            ->withServiceAccountAndApiKey($serviceAccount, $apiKey)
            ->withDatabaseUri('https://sharelook-v3.firebaseio.com')
            ->create();

        $this->db = $firebase->getDatabase();
    }
}
