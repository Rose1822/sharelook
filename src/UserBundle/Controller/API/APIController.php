<?php

namespace UserBundle\Controller\API;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\BaseAPIController;


/**
 * @Route("/api/v1")
 */

class APIController extends BaseAPIController
{
     /**
     * Post signup user
     * 
     * **Response Format**
     *      
     *      {
     *          "status"=>"success",
     *          "uid": 123,
     *      }
     *      {
     *          "status"=>"error",
     *          "error_code": 101,
     *          "error_message":"Insufficient funds in user account",
     *      } 
     * 
     * **Error Code**
     * 
     *      {
     *          101 : "Required parameter not valid"
     *          102 : "Create user account failed"
     *      }  
     * 
     * 
     * @Post("/signup", name="APIUserSignup", options={"method_prefix"=false})
     * @ApiDoc(
     *  section="User",
     *  parameters={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "description"="User email",
     *          "required"=true
     *      },
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "description"="Login name",
     *          "required"=true
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "description"="Password",
     *          "required"=true
     *      },
     *      {
     *          "name"="firstname",
     *          "dataType"="string",
     *          "description"="First name",
     *          "required"=true
     *      },
     *      {
     *          "name"="lastname",
     *          "dataType"="string",
     *          "description"="Last name",
     *          "required"=true
     *      },
     *      {
     *          "name"="mobile",
     *          "dataType"="string",
     *          "description"="Mobile",
     *          "required"=false
     *      },
     *      {
     *          "name"="avatar",
     *          "dataType"="string",
     *          "description"="User avatar",
     *          "required"=false
     *      }
     * 
     *  }
     * )
     */
    public function signupAction()
    {
        $firebase = $this->get('kreait_firebase.connection.main');
        
        $auth = $firebase->getAuth();
        $user = $auth->createUserWithEmailAndPassword('user@domain.tld', '12345');
        $userConnection = $firebase->asUser($user);
        var_dump($userConnection);die;
        $this->db = $firebase->getDatabase();
        
        $newPost = $this->db->getReference('/collection/users')
            ->push([
                'email' => 'noi@3view.com',
                'username'=>'noi',
                'password' => sha1('1234'),
                'firstname' => 'Noi',
                'lastname' => 'Rasee',
                'mobile' => 'Noi',
                'avatar' => 'avatar',
                
            ]);
        
        var_dump($newPost);die;

        
    }
}
